package main

import (
	"bitbucket.org/zaphome/adapterbase/configuration"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/devadapter/api"
	"bitbucket.org/zaphome/devadapter/devactuator"
)

const AdapterName = "Development"

var (
	ctx     *context.Context
	logger  logging.Logger
)

func Initialize(adapt adapters.Adapter) adapters.Adapter {
	adapt.Name = AdapterName

	ctx = configuration.Init(&adapt)
	logger = (*ctx.GetService("Logger")).(logging.Logger)

	// Initialize the REST service
	devactuator.Initialize(ctx)
	service.Initialize(ctx)
	return adapt
}

package service

import (
	"net/http"
	"bitbucket.org/zaphome/devadapter/api/handlers"
)

type Routes []Route

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

const PATH_PREFIX = "/api/v1/"

var routes = Routes{
	Route{
		"Index",
		"GET",
		"",
		handlers.ApiIndex,
	},
	Route{
		"AddActuator",
		"POST",
		"actuator",
		handlers.AddActuatorHandler,
	},
}

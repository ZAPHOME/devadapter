package handlers

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"net/http"
	"fmt"
	"html"
)

var (
	ctx        *context.Context
	adapter    *adapters.Adapter
	messageBus *communication.MessageBus
	logger     *logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	if l, ok := (*ctx.GetService("Logger")).(logging.Logger); ok {
		logger = &l
	}
	if a, ok := (*ctx.GetService("Adapter")).(adapters.Adapter); ok {
		adapter = &a
		messageBus = a.MessageBus
	}
}

func ApiIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

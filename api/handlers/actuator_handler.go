package handlers

import (
	"net/http"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators"
	"io/ioutil"
	"io"
	"encoding/json"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
)

type ActuatorListModel struct {
	Actuators []ActuatorModel `json:"actuators"`
}

type ActuatorModel struct {
	Name  string       `json:"name"`
	State states.State `json:"state"`
}

func AddActuatorHandler(w http.ResponseWriter, r *http.Request) {
	var actuatorModel ActuatorModel
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	logger.ErrorOnError(err)
	logger.ErrorOnError(r.Body.Close())
	if err := json.Unmarshal(body, &actuatorModel); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		logger.ErrorOnError(json.NewEncoder(w).Encode(err))
		return
	}

	actuator := actuators.Actuator{
		Id:        0,
		AdapterId: adapter.Id,
		Name:      actuatorModel.Name,
		State:     actuatorModel.State,
	}
	adapter.MessageBus.SourceChannel.SendMessage(
		actuators.BuildActuatorConnectedEvent(adapter.Id, actuator),
	)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	logger.ErrorOnError(
		json.NewEncoder(w).Encode(ActuatorModel{
			Name:  actuatorModel.Name,
			State: actuatorModel.State,
		}),
	)
}

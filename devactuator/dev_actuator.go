package devactuator

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"reflect"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
)

var (
	ctx        *context.Context
	adapter    *adapters.Adapter
	messageBus *communication.MessageBus
	logger     logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	logger = (*ctx.GetService("Logger")).(logging.Logger)
	if a, ok := (*ctx.GetService("Adapter")).(adapters.Adapter); ok {
		adapter = &a
		messageBus = a.MessageBus
		messageBus.DestinationChannel.AddObserverFunction(
			reflect.TypeOf(&states.ActuatorStateChangeInstruction{}),
			processActuatorChangeMessage,
		)
	} else {
		logger.Error("Failed to initialize development actuators.")
	}
}

func processActuatorChangeMessage(message messaging.Message) {
	// The development actuator does nothing on change.
	// Only the central will be informed to save the new actuator state.
	instruction := message.(*states.ActuatorStateChangeInstruction)
	changedEvent := states.BuildActuatorStateChangedEvent(adapter.Id, instruction.ActuatorId, instruction.State)
	adapter.MessageBus.SourceChannel.SendMessage(changedEvent)
}
